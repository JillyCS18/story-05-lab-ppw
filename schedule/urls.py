from django.urls import path
from . import views

app_name = "schedule"

urlpatterns = [
    path('', views.homepage, name='homepage'),
    path('about/', views.about, name='about'),
    path('schedule/', views.schedule, name='schedule'),
    path('schedule/<int:pk>', views.delete_schedule, name='delete_schedule'),
    path('skills/', views.skills, name='skills'),
    path('exp/', views.exp, name='exp'),
    path('contact/', views.contact, name='contact'),
    path('about/swot/', views.swot, name='swot'),
]
