from django.db import models

# Create your models here.

class Schedule(models.Model):
    activity = models.CharField(max_length=100)
    place = models.CharField(max_length=100)
    category = models.CharField(max_length=100)
    datetime = models.DateTimeField()

    def __str__(self):
        return self.activity
