from django.shortcuts import render
from .models import Schedule
from django.shortcuts import redirect

# Create your views here.
def homepage(request):
    return render(request, 'Homepage.html')

def about(request):
    return render(request, 'About.html')

def schedule(request):
    list_of_schedule = Schedule.objects.all().order_by('datetime')
    if request.method == 'POST':
        activity_r = request.POST.get('activity')
        place_r = request.POST.get('place')
        category_r = request.POST.get('category')
        datetime_r = request.POST.get('datetime')

        schedule = Schedule(activity=activity_r, place=place_r, category=category_r, datetime=datetime_r)
        schedule.save()
        return render(request, 'Schedule.html', {'schedules' : list_of_schedule})
    else:
        return render(request, 'Schedule.html', {'schedules' : list_of_schedule})

def delete_schedule(request, pk):
    list_of_schedule = Schedule.objects.all()
    obj = list_of_schedule.get(id=pk)
    obj.delete()
    return redirect('/schedule/')

def skills(request):
    return render(request, 'Skills.html')

def exp(request):
    return render(request, 'Experiences.html')

def contact(request):
    return render(request, 'Contact.html')

def swot(request):
    return render(request, 'Swot.html')
